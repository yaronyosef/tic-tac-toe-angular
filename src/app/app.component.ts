import { Component } from '@angular/core';
import { debug } from 'util';

enum Symbol { O, X }

enum GameStatus { Initial, Won }

interface Square {
  value: number;
  symbol?: Symbol;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  readonly winningMatch: number = 15;

  readonly players: Array<Symbol> = [Symbol.X, Symbol.O];

  currentPlayerSymbol: Symbol = this.players[Math.floor(Math.random() * Math.floor(2))];

  gameStatus: GameStatus = GameStatus.Initial;

  strikingSquares: Array<number> = [];

  squares: Array<Square> = [
    { value: 2 }, { value: 7 }, { value: 6 },
    { value: 9 }, { value: 5 }, { value: 1 },
    { value: 4 }, { value: 3 }, { value: 8 }
  ];

  reset(): void {
    this.gameStatus = GameStatus.Initial;
    this.squares.forEach(square => delete square.symbol);
    this.strikingSquares = [];
  }

  squareClicked(index): boolean {
    if (!this.isValidMove(index) || this.gameWon()) { return; }
    const remainingNumbersToWin: number = this.winningMatch - this.squares[index].value;
    const pairSum: Array<number> = this.getPairSumOf(remainingNumbersToWin);
    this.squares[index].symbol = this.currentPlayerSymbol;
    if (pairSum.length > 0) {
      this.gameStatus = GameStatus.Won;
      this.strikingSquares = [...pairSum, this.squares[index].value];
    } else {
      this.switchPlayer();
    }
    return true;
  }

  switchPlayer(): void {
    this.currentPlayerSymbol = Boolean(this.currentPlayerSymbol) ? Symbol.O : Symbol.X;
  }

  isValidMove(index): boolean {
    return this.squares[index].symbol === undefined;
  }

  getCurrentPlayerSquaresIds(): Array<number> {
    return this.squares.
      filter(square => square.symbol === this.currentPlayerSymbol).
      map(square => square.value);
  }

  getPairSumOf(remainingNumbersToWin): Array<number> {
    const currentPlayerSquares: Array<number> = this.getCurrentPlayerSquaresIds().sort();
    let pairSum: Array<number> = [];
    if (currentPlayerSquares.length === 0) { return pairSum; }
    let leftPosition: number = 0;
    let rightPosition: number = currentPlayerSquares.length - 1;
    while (leftPosition < rightPosition) {
      const number1: number = currentPlayerSquares[leftPosition];
      const number2: number = currentPlayerSquares[rightPosition];
      const sumOfSquares: number = number1 + number2;
      if (sumOfSquares === remainingNumbersToWin) {
        pairSum = [number1, number2];
        break;
      } else if (sumOfSquares < remainingNumbersToWin) {
        leftPosition++;
      } else {
        rightPosition--;
      }
    }
    return pairSum;
  }

  gameWon(): boolean {
    return this.gameStatus === GameStatus.Won;
  }

  getSymbolString(symbol): String {
    if (symbol === Symbol.O) {
      return 'O';
    } else if (symbol === Symbol.X) {
      return 'X';
    }
    return '';
  }

  getCurrentPlayerSymbol(): String {
    return this.getSymbolString(this.currentPlayerSymbol);
  }

}
